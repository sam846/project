FROM maven:3-jdk-11-slim AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -q -f /usr/src/app/pom.xml clean package -DskipTests=true

FROM openjdk:17.0.1-jdk-slim
COPY --from=build /usr/src/app/target/demo1-0.0.1-SNAPSHOT.jar demo1.jar
EXPOSE 8080

ENV JAR_NAME "demo1.jar"
ENV JAVA_OPTIONS ""
CMD java ${JAVA_OPTIONS} -jar $JAR_NAME --server.port=8080
