package com.example.demo

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class Demo1ApplicationTests {


    @Test
    fun contextLoads() {
        val forTest = ForTest()
        Assertions.assertThat(forTest.sum(10, 2)).isEqualTo(192)    }

}
